#!"D:\STUDIA\Rok 3\Semestr 6\Programowanie w internecie\lab9\djangoenv\Scripts\python.exe"
# EASY-INSTALL-ENTRY-SCRIPT: 'djangocms-installer==0.4.1','console_scripts','djangocms'
__requires__ = 'djangocms-installer==0.4.1'
import sys
from pkg_resources import load_entry_point

if __name__ == '__main__':
    sys.exit(
        load_entry_point('djangocms-installer==0.4.1', 'console_scripts', 'djangocms')()
    )
