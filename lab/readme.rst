Laboratorium #9
===============

1. W django-cms stworzyć zarys strony domowej z następującą (lub podobną) strukturą drzewiastą uwidocznioną w menu:

   * Strona główna / powitalna
   * Zainteresowania – zgrubny opis

     * zainteresowanie 1 szczegółowo
     * zainteresowanie 2 szczegółowo
     * ...

   * Kontakt

   Każda ze stron pierwszego poziomu powinna mieć inny układ. Stworzyć co najmniej jeden własny układ.


2. Stworzyć konfigurowalną wtyczkę do wstawienia `przycisku "like" <https://developers.facebook.com/docs/reference/plugins/like/>`__.
   Wstawić przycisk na stronach poszczególnych zainteresowań (lub adekwatnych).


Całość powinna działać jako aplikacja wsgi zarządzana przez *uwsgi* pod *nginx*-em na serwerze laboratoryjnym.
